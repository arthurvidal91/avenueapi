package br.com.avenueapi.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenueapi.dao.ProductDAO;
import br.com.avenueapi.dao.impl.ProductDAOImpl;
import br.com.avenueapi.filter.ProductFilterDTO;
import br.com.avenueapi.model.Product;

@Path("/product")
public class ProductService {

	static {
		ProductDAOImpl.initEmps();
	}

	@Path("nofilter")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getProducts() {
		List<Product> listAllProducts = new ProductDAOImpl().findAll();
		Response response = Response.status(200).entity("No products").build();
		if (listAllProducts != null && !listAllProducts.isEmpty()) {
			response = Response.status(200).entity(listAllProducts).build();
		}
		return response;
	}

	@Path("nofilter/{prodId}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getProductsById(@PathParam("prodId") Long prodId) {
		Product product = new ProductDAOImpl().findById(prodId);
		Response response = Response.status(200).entity("No product with id = " + prodId).build();
		if (product != null) {
			response = Response.status(200).entity(product).build();
		}
		return response;
	}

	@Path("withfilter")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getProductsWithFilter() {
		List<Product> listAllProducts = new ProductDAOImpl().findAll();
		List<ProductFilterDTO> listAllProductsFiltered = new ArrayList<>();
		for (Product product : listAllProducts) {
			ProductFilterDTO productFiltered = new ProductFilterDTO(product);
			listAllProductsFiltered.add(productFiltered);
		}
		Response response = Response.status(200).entity("No products").build();
		if (!listAllProductsFiltered.isEmpty()) {
			response = Response.status(200).entity(listAllProductsFiltered).build();
		}

		return response;
	}

	@Path("withfilter/{prodId}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getProductsWithFilterById(@PathParam("prodId") Long prodId) {
		Product product = new ProductDAOImpl().findById(prodId);
		Response response = Response.status(200).entity("No product with id = " + prodId).build();
		if (product != null) {
			ProductFilterDTO productFiltered = new ProductFilterDTO(product);
			response = Response.status(200).entity(productFiltered).build();
		}
		return response;
	}

	@Path("childproduct/{prodId}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getChildProductsByProductId(@PathParam("prodId") Long prodId) {
		Product product = new ProductDAOImpl().findById(prodId);
		Response response = Response.status(200).entity("No product with id = " + prodId).build();
		if (product != null) {
			response = Response.status(200).entity(product.getProductsChild()).build();
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Product addProduct(Product emp) {
		emp.refreshLists();
		return new ProductDAOImpl().merge(emp);
	}

	@DELETE
	@Path("/{prodId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteProduct(@PathParam("prodId") Long prodId) {
		Response response = Response.status(200).entity("Product with id = " + prodId + " removed.").build();
		ProductDAO dao = new ProductDAOImpl();
		Product product = dao.findById(prodId);
		if (product == null) {
			response = Response.status(200).entity("No products with id = " + prodId).build();
		} else {
			try {
				new ProductDAOImpl().delete(prodId);
			} catch (Exception e) {
				response = Response.status(417).entity("Problems removing product").build();
			}
		}
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	public Product updateProduct(Product emp) {
		emp.refreshLists();
		return new ProductDAOImpl().merge(emp);
	}

}