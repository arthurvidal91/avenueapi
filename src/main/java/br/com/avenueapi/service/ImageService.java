package br.com.avenueapi.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.avenueapi.dao.impl.ImageDAOImpl;
import br.com.avenueapi.dao.impl.ProductDAOImpl;
import br.com.avenueapi.model.Image;
import br.com.avenueapi.model.Product;

@Path("/image")
public class ImageService {

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getImages() {
		List<Image> listAllImages = new ImageDAOImpl().findAll();
		Response response = Response.status(200).entity("No images").build();
		if (listAllImages != null && !listAllImages.isEmpty()) {
			response = Response.status(200).entity(listAllImages).build();
		}
		return response;

	}

	@Path("/{prodId}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getImageByProductId(@PathParam("prodId") Long prodId) {
		Product product = new ProductDAOImpl().findById(prodId);
		Response response = Response.status(200).entity("No product with id = " + prodId).build();
		if (product != null) {
			response = Response.status(200).entity(product.getImages()).build();
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	public Image addImage(Image image) {
		return new ImageDAOImpl().merge(image);
	}

	@DELETE
	@Path("/{prodId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public void deleteImage(@PathParam("prodId") Long prodId) {
		new ImageDAOImpl().delete(prodId);
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	public Image updateImage(Image emp) {
		return new ImageDAOImpl().merge(emp);
	}

}
