package br.com.avenueapi.dao;

import br.com.avenueapi.model.Product;

public interface ProductDAO extends GenericDAO<Product> {

}

//1) Create, update and delete products
//2) Create, update and delete images
//3) Get all products excluding relationships (child products, images) 
//4) Get all products including specified relationships (child product and/or images) 
//5) Same as 3 using specific product identity 
//6) Same as 4 using specific product identity 
//7) Get set of child products for specific product 
//8) Get set of images for specific product\