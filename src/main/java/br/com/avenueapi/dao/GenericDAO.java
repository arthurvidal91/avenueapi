package br.com.avenueapi.dao;

import java.util.List;

public interface GenericDAO<T> {

	T findById(Long id);

	List<T> findAll();
	
	T merge(T entity);
	
	void delete(Long entity);
}