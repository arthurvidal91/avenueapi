package br.com.avenueapi.dao.impl;

import br.com.avenueapi.dao.ImageDAO;
import br.com.avenueapi.model.Image;

public class ImageDAOImpl extends GenericDAOImpl<Image> implements ImageDAO {

	public ImageDAOImpl() {
		super(Image.class);
	}

}