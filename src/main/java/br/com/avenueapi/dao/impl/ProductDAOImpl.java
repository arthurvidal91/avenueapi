package br.com.avenueapi.dao.impl;

import br.com.avenueapi.dao.ProductDAO;
import br.com.avenueapi.model.Image;
import br.com.avenueapi.model.Product;

public class ProductDAOImpl extends GenericDAOImpl<Product> implements ProductDAO {
	
	public static void initEmps() {
		Product product1 = new Product("Product 1","Desc 1");
		Product product2 = new Product("Product 2","Desc 2");
		Product product3 = new Product("Product 3","Desc3");

		Image image1 = new Image("Type 1");
		Image image2 = new Image("Type 2");
		product1.add(image1);
		product1.add(product2);
		product2.add(image2);

		ProductDAOImpl productDAO = new ProductDAOImpl();
		productDAO.merge(product1);
		productDAO.merge(product3);
	}

	public ProductDAOImpl() {
		super(Product.class);
	}

}