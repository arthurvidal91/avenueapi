package br.com.avenueapi.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.avenueapi.connection.ConnectionFactory;
import br.com.avenueapi.dao.GenericDAO;

public class GenericDAOImpl<T> implements GenericDAO<T> {

	private final ConnectionFactory connection = ConnectionFactory.getConnection();
	private final Class<T> type;
	private final String name;

	protected GenericDAOImpl(Class<T> type) {
		this.type = type;
		name = type.getSimpleName();
	}

	@Override
	public T merge(T entity) {
		try {
			getEntityManager().getTransaction().begin();
			entity = getEntityManager().merge(entity);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();

		} catch (Exception e) {
			getEntityManager().getTransaction().rollback();
			throw e;
		} finally {
			connection.closeConnection();
		}
		return entity;
	}

	@Override
	public T findById(Long id) {
		T entity = getEntityManager().find(type, id);
		connection.closeConnection();
		return entity;
	}

	@Override
	public List<T> findAll() {
		List<T> all = getEntityManager().createQuery("SELECT a FROM " + name + " a").getResultList();
		connection.closeConnection();
		return all;
	}

	@Override
	public void delete(Long id) {
		try {
			T entity = findById(id);
			getEntityManager().getTransaction().begin();
			entity = getEntityManager().merge(entity);
			getEntityManager().remove(entity);
			getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			getEntityManager().getTransaction().rollback();
			throw e;
		} finally {
			connection.closeConnection();
		}
	}

	public EntityManager getEntityManager() {
		return connection.getEntityManager();
	}

}