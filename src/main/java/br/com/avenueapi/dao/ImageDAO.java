package br.com.avenueapi.dao;

import br.com.avenueapi.model.Image;

public interface ImageDAO extends GenericDAO<Image> {

}
