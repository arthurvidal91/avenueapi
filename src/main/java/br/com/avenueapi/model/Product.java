package br.com.avenueapi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

@Entity
@Table(name = "Product")
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProduct;

	@Column(name = "NM_PRODUCT")
	private String name;

	@Column(name = "DS_PRODUCT")
	private String description;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "ID_PRODUCT_PARENT")
	private Product productParent;

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "productParent", orphanRemoval = true, cascade = { CascadeType.ALL })
	private List<Product> productsChild;

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", orphanRemoval = true, cascade = { CascadeType.ALL })
	private List<Image> images;

	public Product(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Product() {
		super();
	}

	public void add(Image image) {
		image.setProduct(this);
		getImages().add(image);
	}

	public void add(Product product) {
		product.setProductParent(this);
		getProductsChild().add(product);
	}

	public void refreshLists() {
		this.getImages();
		this.getProductsChild();
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getProductParent() {
		return productParent;
	}

	public void setProductParent(Product productParent) {
		this.productParent = productParent;
	}

	public List<Product> getProductsChild() {
		if (productsChild == null) {
			productsChild = new ArrayList<>();
		}
		return productsChild;
	}

	public void setProductsChild(List<Product> productsChild) {
		this.productsChild = productsChild;
	}

	public List<Image> getImages() {
		if (images == null) {
			images = new ArrayList<>();
		}
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

}