package br.com.avenueapi.filter;

import br.com.avenueapi.model.Product;

public class ProductFilterDTO {

	private Long idProduct;

	private String name;

	private String description;

	public ProductFilterDTO(Product product) {
		setName(product.getName());
		setDescription(product.getDescription());
		setIdProduct(product.getIdProduct());
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
