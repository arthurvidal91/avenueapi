package br.com.avenueapi.connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.avenueapi.dao.impl.ProductDAOImpl;

public class ConnectionFactory {
	
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	private static ConnectionFactory connection = new ConnectionFactory();
	
	public static ConnectionFactory getConnection() {
		return connection;
	}
	
	public EntityManager getEntityManager() {
		if(entityManagerFactory == null || !entityManagerFactory.isOpen()){
			entityManagerFactory = Persistence.createEntityManagerFactory("AvenueAPI");
		}
		
		if(entityManager == null || !entityManager.isOpen()){
			entityManager = entityManagerFactory.createEntityManager(); 
		}
		
		return entityManager;
	}
	
	public EntityManagerFactory getEntityManagerFactory(){
		return entityManagerFactory;
	}
	
	public void closeConnection(){
		if(entityManager!=null || entityManager.isOpen()){
			this.entityManager.close();
		}
	}
	
}