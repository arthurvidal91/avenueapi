package br.com.avenueapi.teste.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.avenueapi.dao.ImageDAO;
import br.com.avenueapi.dao.impl.ImageDAOImpl;
import br.com.avenueapi.dao.impl.ProductDAOImpl;
import br.com.avenueapi.model.Image;
import br.com.avenueapi.model.Product;

public class ImageDAOTest {

	private ImageDAO dao;

	@Before
	public void init() {
		dao = new ImageDAOImpl();
	}

	@Test
	public void addImage() {
		Image image = new Image();
		image = dao.merge(image);
		Assert.assertNotNull("Imagem não inserida", image.getIdImage());
	}

	@Test
	public void listAllImages() {
		List<Image> allImages = dao.findAll();
		Assert.assertNotNull("Nenhuma imagem retornada", allImages);
	}

	@Test
	public void findImageByID() {
		Image image = new Image();
		image = dao.merge(image);

		Image imageSaved = dao.findById(image.getIdImage());
		Assert.assertNotNull("Imagem não resgatada", imageSaved.getIdImage());
	}

	@Test
	public void deleteImageByID() {
		Image image = new Image();
		image = dao.merge(image);
		dao.delete(image.getIdImage());

		image = dao.findById(image.getIdImage());
		Assert.assertNull("Imagem não deletada", image);
	}
}
