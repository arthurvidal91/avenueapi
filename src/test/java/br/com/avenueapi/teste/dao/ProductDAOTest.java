package br.com.avenueapi.teste.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.avenueapi.dao.ProductDAO;
import br.com.avenueapi.dao.impl.ProductDAOImpl;
import br.com.avenueapi.model.Product;

public class ProductDAOTest {

	private ProductDAO dao;

	@Before
	public void init() {
		dao = new ProductDAOImpl();
	}

	@Test
	public void addProduct() {
		Product product = new Product();
		product = dao.merge(product);
		Assert.assertNotNull("Produto não inserido", product.getIdProduct());
	}

	@Test
	public void listAllProduct() {
		List<Product> allProducts = dao.findAll();
		Assert.assertNotNull("Nenhum produto retornado", allProducts);
	}

	@Test
	public void findProductByID() {
		Product product = new Product();
		product = dao.merge(product);

		Product productSaved = dao.findById(product.getIdProduct());
		Assert.assertNotNull("Produto não resgatado", productSaved.getIdProduct());
	}

	@Test
	public void deleteProductByID() {
		Product product = new Product();
		product = dao.merge(product);

		dao.delete(product.getIdProduct());
		product = dao.findById(product.getIdProduct());
		Assert.assertNull("Produto não deletado", product);
	}
}
