# AvenueAPI #

Restful service using JAX-RS to perform CRUD operations on a Product resource using Image as a sub-resource of Product.

### How to compile and run the application? ###

* run "mvn clean install"
* After the build run "mvn tomcat:run"


### How to run automated tests  ###

* run "mvn test"

### Json Body ###

Product:

    {
       "name": "xxxxxx",
       "description": "xxxxxx"
       "productsChild": [{ 
			"name": "xxxxxx",
			"description": "xxxxxxx",
			"productsChild": [],
			"images": []
    	}],
    	"images": [{
			"type": "xxxxx"
    	}]
    }

Image:

    {
      "type": "xxxxx"
    }

### Calls examples ###

1) Create product -  call method POST passing product's json to http://localhost:8080/AvenueAPI/rest/product/    
2) Update product -  call method PUT passing product's json to http://localhost:8080/AvenueAPI/rest/product/    
3) Delete product - call method DELETE passing product's id to http://localhost:8080/AvenueAPI/rest/product/1    

4) Create image - call method POST passing image's json to http://localhost:8080/AvenueAPI/rest/image/    
5) Update image - call method PUT passing image's json to http://localhost:8080/AvenueAPI/rest/image/    
6) Delete image - call method DELETE passing images's id to http://localhost:8080/AvenueAPI/rest/product/1    

7) Get all products excluding relationships - call method GET http://localhost:8080/AvenueAPI/rest/product/withfilter     
8) Get all products including specified relationships - call method GET http://localhost:8080/AvenueAPI/rest/product/nofilter    

9) Same as 7 using specific product identity - call method GET http://localhost:8080/AvenueAPI/rest/product/withfilter    
10) Same as 8 using specific product identity - call method GET http://localhost:8080/AvenueAPI/rest/product/nofilter    

11) Get set of child products for specific product - call method GET http://localhost:8080/AvenueAPI/rest/product/childproduct/1    
12) Get set of images for specific product - call method GET http://localhost:8080/AvenueAPI/rest/image/1    
